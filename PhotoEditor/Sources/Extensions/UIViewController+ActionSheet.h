//
//  UIViewController+ActionSheet.h
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (ActionSheet)

- (void)showObtainPhotoActionSheetWithActions:(NSArray *)actions;
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message;

@end
