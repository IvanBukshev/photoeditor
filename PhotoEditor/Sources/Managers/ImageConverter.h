//
//  ImageConverter.h
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "ImageConverterProtocol.h"

@interface ImageConverter : NSObject <ImageConverterProtocol>

@end
