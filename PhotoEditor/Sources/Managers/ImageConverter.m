//
//  ImageConverter.m
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "ImageConverter.h"

@implementation ImageConverter

- (UIImage *)transformImage:(UIImage *)image transformation:(id<BaseTransformationProtocol>)transformation {
    return [transformation performImage:image];
}

@end
