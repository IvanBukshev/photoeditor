//
//  ImageConverterProtocol.h
//  PhotoEditor
//
//  Created by Иван Букшев on 22/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "BaseTransformationProtocol.h"

@protocol ImageConverterProtocol <NSObject>

- (UIImage *)transformImage:(UIImage *)image
             transformation:(id<BaseTransformationProtocol>)transformation;

@end
