//
//  AppAssembly.h
//  PhotoEditor
//
//  Created by Иван Букшев on 22/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppAssembly : NSObject

+ (void)installRootViewController:(UIWindow *)window;

@end
