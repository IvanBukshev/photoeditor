//
//  AppAssembly.m
//  PhotoEditor
//
//  Created by Иван Букшев on 22/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "AppAssembly.h"
// Home module.
#import "HomeViewController.h"
#import "HomeViewModel.h"
#import "ImageConverter.h"

@implementation AppAssembly

+ (void)installRootViewController:(UIWindow *)window {
    ImageConverter *imageConverter = [ImageConverter new];
    
    HomeViewController *viewController = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    HomeViewModel *viewModel = [[HomeViewModel alloc] initWithImageConverter:imageConverter];
    viewModel.view = viewController;
    viewController.viewModel = viewModel;
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    window.rootViewController = navigationController;
    [window makeKeyAndVisible];
}

@end
