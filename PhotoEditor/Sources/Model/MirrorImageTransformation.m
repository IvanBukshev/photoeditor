//
//  MirrorImageTransformation.m
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "MirrorImageTransformation.h"

@implementation MirrorImageTransformation

#pragma mark - BaseTransformationProtocol
- (UIImage *)performImage:(UIImage *)image {
    UIImageOrientation flippedOrientation = UIImageOrientationUp;
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
            flippedOrientation = UIImageOrientationDownMirrored;
            break;
        case UIImageOrientationLeft:
            flippedOrientation = UIImageOrientationLeftMirrored;
            break;
        case UIImageOrientationUp:
            flippedOrientation = UIImageOrientationUpMirrored;
            break;
        case UIImageOrientationRight:
            flippedOrientation = UIImageOrientationRightMirrored;
            break;
        case UIImageOrientationDownMirrored:
            flippedOrientation = UIImageOrientationDown;
            break;
        case UIImageOrientationLeftMirrored:
            flippedOrientation = UIImageOrientationLeft;
            break;
        case UIImageOrientationUpMirrored:
            flippedOrientation = UIImageOrientationUp;
            break;
        case UIImageOrientationRightMirrored:
            flippedOrientation = UIImageOrientationRight;
            break;
    }
    
    return [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:flippedOrientation];
}

@end
