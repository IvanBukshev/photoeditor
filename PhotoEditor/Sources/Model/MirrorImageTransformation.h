//
//  MirrorImageTransformation.h
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "BaseTransformationProtocol.h"

@interface MirrorImageTransformation : NSObject <BaseTransformationProtocol>

@end
