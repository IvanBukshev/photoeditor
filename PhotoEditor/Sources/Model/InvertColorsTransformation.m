//
//  InvertColorsTransformation.m
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "InvertColorsTransformation.h"

@implementation InvertColorsTransformation

#pragma mark - BaseTransformationProtocol
- (UIImage *)performImage:(UIImage *)image {
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    CGContextDrawImage(context, imageRect, [image CGImage]);
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);

    return newImage;
}

@end
