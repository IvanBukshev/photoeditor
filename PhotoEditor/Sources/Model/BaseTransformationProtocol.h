//
//  BaseTransformationProtocol.h
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BaseTransformationProtocol <NSObject>

- (UIImage *)performImage:(UIImage *)image;

@end
