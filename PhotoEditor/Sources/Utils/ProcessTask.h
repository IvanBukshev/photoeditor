//
//  ProcessTask.h
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProcessTask : NSObject

- (void)startProcessWithCallback:(void (^)(float))callback;
- (float)currentProgress;

@end
