//
//  ProcessTask.m
//  PhotoEditor
//
//  Created by Иван Букшев on 21/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "ProcessTask.h"

@interface ProcessTask ()
@property (nonatomic, copy) void (^callback)(float progress);
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, assign) float step;
@property (nonatomic, assign) float progress;
@property (nonatomic, assign) NSInteger stepsAmount;
@property (nonatomic, assign) NSInteger currentStep;
@end

@implementation ProcessTask

#pragma mark - Initialization
- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _stepsAmount = 5 + arc4random_uniform(26);
    _currentStep = 0;
    _step = 1.0f / _stepsAmount;
    _progress = 0.0f;
    
    return self;
}

#pragma mark - Public Methods
- (void)startProcessWithCallback:(void (^)(float))callback {
    if (!self.timer && self.currentStep < 1) {
        self.callback = callback;
        self.timer = [NSTimer timerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(incrementProgress)
                                           userInfo:nil
                                            repeats:YES];
        
        [[NSRunLoop mainRunLoop] addTimer:self.timer
                                  forMode:NSRunLoopCommonModes];
    }
}

#pragma mark - Private Helpers
- (void)incrementProgress {
    self.progress = self.step * self.currentStep++;
    self.callback(self.progress);
    
    if (self.progress >= 1.0f && [self.timer isValid]) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (float)currentProgress {
    return self.progress;
}

@end
