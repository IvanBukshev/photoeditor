//
//  HomeConfigurableProtocol.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeTableItemProtocol.h"

@protocol HomeConfigurableProtocol <NSObject>

@required
@property (nonatomic, strong) id<HomeTableItemProtocol> item;

@optional
- (void)configureWithItem:(id<HomeTableItemProtocol>)item;
- (void)configureWithItem:(id<HomeTableItemProtocol>)item
               completion:(void (^)(BOOL))completion;

@end
