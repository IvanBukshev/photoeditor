//
//  ImageTableCell.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeConfigurableProtocol.h"

@interface ImageTableCell : UITableViewCell <HomeConfigurableProtocol>

@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@end
