//
//  ImageTableCell.m
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "ImageTableCell.h"

@implementation ImageTableCell
@synthesize item = _item, imageView;

#pragma mark - HomeConfigurableProtocol
- (void)configureWithItem:(id<HomeTableItemProtocol>)item {
    self.item = item;
    if (!!item.image) {
        self.imageView.image = item.image;
    }
}

@end
