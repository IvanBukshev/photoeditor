//
//  ProgressTableCell.m
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "ProgressTableCell.h"

@implementation ProgressTableCell
@synthesize item = _item, progressView;

#pragma mark - HomeConfigurableProtocol
- (void)configureWithItem:(id<HomeTableItemProtocol>)item completion:(void (^)(BOOL))completion {
    self.item = item;
    [item startProcessWithCallback:^(float progress) {
        self.progressView.progress = progress;
        if (progress >= 1.0f) {
            item.processed = YES;
            completion(YES);
        }
    }];
}

@end
