//
//  HomeTableItem.m
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeTableItem.h"
#import "ProcessTask.h"

@interface HomeTableItem ()
@property (nonatomic, strong) ProcessTask *processTask;
@end

@implementation HomeTableItem
@synthesize processed, image;

#pragma mark - Initialization
- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _processTask = [ProcessTask new];
    self.processed = NO;
    
    return self;
}

#pragma mark - Public Methods
- (void)startProcessWithCallback:(void (^)(float))callback; {
    [self.processTask startProcessWithCallback:callback];
}

- (float)currentProgress {
    return [self.processTask currentProgress];
}

@end
