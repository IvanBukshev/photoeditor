//
//  HomeTableItemProtocol.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeTableItemProtocol <NSObject>

@optional
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, assign, getter=isProcessed) BOOL processed;

- (void)startProcessWithCallback:(void (^)(float))callback;
- (float)currentProgress;

@end
