//
//  HomeViewModel.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeViewOutput.h"

@protocol HomeViewInput;
@protocol ImageConverterProtocol;

@interface HomeViewModel : NSObject <HomeViewOutput>

@property (nonatomic, weak) id<HomeViewInput> view;

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithImageConverter:(id<ImageConverterProtocol>)converter NS_DESIGNATED_INITIALIZER;

@end
