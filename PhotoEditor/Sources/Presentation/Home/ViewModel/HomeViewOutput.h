//
//  HomeViewOutput.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIImage.h>

@protocol HomeTableItemProtocol;

@protocol HomeViewOutput <NSObject>

- (void)triggerViewReadyEvent;

- (void)triggerRotateImageAction:(UIImage *)image;
- (void)triggerInvertImageColorsAction:(UIImage *)image;
- (void)triggerMirrorImageAction:(UIImage *)image;

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)numberOfRows;
- (id<HomeTableItemProtocol>)itemAtIndexPath:(NSIndexPath *)indexPath;
- (void)deleteItemAtIndexPath:(NSIndexPath *)indexPath;
- (NSIndexPath *)indexPathForItem:(id<HomeTableItemProtocol>)item;

@end
