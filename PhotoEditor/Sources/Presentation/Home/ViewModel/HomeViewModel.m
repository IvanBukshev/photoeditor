//
//  HomeViewModel.m
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeViewModel.h"
// View.
#import "HomeViewInput.h"
// Converter with tranformations.
#import "ImageConverterProtocol.h"
#import "RotateTransformation.h"
#import "InvertColorsTransformation.h"
#import "MirrorImageTransformation.h"
// For indexPath.
#import <UIKit/UITableView.h>
// Items.
#import "HomeTableItem.h"

@interface HomeViewModel ()
@property (nonatomic, strong) id<ImageConverterProtocol> converter;
@property (nonatomic, strong) NSMutableArray<id<HomeTableItemProtocol>> *items;
@end

@implementation HomeViewModel
@synthesize view;

#pragma mark - Initialization
- (instancetype)initWithImageConverter:(id<ImageConverterProtocol>)converter {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _converter = converter;
    _items = [NSMutableArray array];
    
    return self;
}

#pragma mark - HomeViewOutput
- (void)triggerViewReadyEvent {
    [self.view setupInitialState];
}

- (void)triggerRotateImageAction:(UIImage *)image {
    RotateTransformation *transformation = [RotateTransformation new];
    [self addItemWithTransformedImage:[self.converter transformImage:image transformation:transformation]];
}

- (void)triggerInvertImageColorsAction:(UIImage *)image {
    InvertColorsTransformation *transformation = [InvertColorsTransformation new];
    [self addItemWithTransformedImage:[self.converter transformImage:image transformation:transformation]];
}

- (void)triggerMirrorImageAction:(UIImage *)image {
    MirrorImageTransformation *transformation = [MirrorImageTransformation new];
    [self addItemWithTransformedImage:[self.converter transformImage:image transformation:transformation]];
}

- (void)addItemWithTransformedImage:(UIImage *)transformedImage {
    HomeTableItem *item = [[HomeTableItem alloc] init];
    item.image = transformedImage;
    [self.items addObject:item];
    [self.view reloadTable];
}

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.items objectAtIndex:indexPath.row].isProcessed) {
        return 256.0f;
    } else {
        return 44.0f;
    }
}

- (NSInteger)numberOfRows {
    return [self.items count];
}

- (id<HomeTableItemProtocol>)itemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.items objectAtIndex:indexPath.row];
}

- (void)deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.items removeObjectAtIndex:indexPath.row];
}

- (NSIndexPath *)indexPathForItem:(id<HomeTableItemProtocol>)item {
    return [NSIndexPath indexPathForRow:[self.items indexOfObject:item] inSection:0];
}

@end
