//
//  HomeViewController.m
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeViewController.h"
#import "UIViewController+ActionSheet.h"
// View model.
#import "HomeViewOutput.h"
// Item.
#import "HomeTableItemProtocol.h"
// Cells.
#import "ProgressTableCell.h"
#import "ImageTableCell.h"

static NSString *const kProgressCellID = @"kProgressCellID";
static NSString *const kImageCellID = @"kImageCellID";

@interface HomeViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@end

@implementation HomeViewController
@synthesize viewModel;

#pragma mark - Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [viewModel triggerViewReadyEvent];
}

#pragma mark - User Actions
- (IBAction)triggerOpenActionSheet {
    __weak typeof(self) wSelf = self;
    NSMutableArray *actions = [NSMutableArray arrayWithCapacity:2];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Камера" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            __strong typeof(wSelf) sSelf = wSelf;
            if (!!sSelf) {
                [sSelf presentImagePicker:UIImagePickerControllerSourceTypeCamera];
            }
        }];
        [actions addObject:camera];
    }

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        UIAlertAction *library = [UIAlertAction actionWithTitle:@"Галерея" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            __strong typeof(wSelf) sSelf = wSelf;
            if (!!sSelf) {
                [sSelf presentImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
            }
        }];
        [actions addObject:library];
    }
    
    [self showObtainPhotoActionSheetWithActions:[actions copy]];
}

- (IBAction)triggerRotateAction {
    if (!!self.imageView.image) {
        [self.viewModel triggerRotateImageAction:self.imageView.image];
    } else {
        [self showImageMissingAlert];
    }
}

- (IBAction)triggerInvertColorsAction {
    if (!!self.imageView.image) {
        [self.viewModel triggerInvertImageColorsAction:self.imageView.image];
    } else {
        [self showImageMissingAlert];
    }
}

- (IBAction)triggerMirrorImageAction {
    if (!!self.imageView.image) {
        [self.viewModel triggerMirrorImageAction:self.imageView.image];
    } else {
        [self showImageMissingAlert];
    }
}

- (void)showImageMissingAlert {
    [self showAlertWithTitle:@"Изображение отсутствует" message:@"Выберите изображение для обработки"];
}

#pragma mark - HomeViewInput
- (void)setupInitialState {
    self.title = @"Фоторедактор";
    
    self.imagePicker = [UIImagePickerController new];
    self.imagePicker.allowsEditing = NO;
    self.imagePicker.delegate = self;
    
    UINib *progressCellNib = [UINib nibWithNibName:@"ProgressTableCell" bundle:nil];
    UINib *imageCellNib = [UINib nibWithNibName:@"ImageTableCell" bundle:nil];
    [self.tableView registerNib:progressCellNib forCellReuseIdentifier:kProgressCellID];
    [self.tableView registerNib:imageCellNib forCellReuseIdentifier:kImageCellID];
}

- (void)reloadTable {
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell<HomeConfigurableProtocol> *cell = nil;
    // Show ProcessTableCell, if item is not already processed.
    // Else: load ImageTableCell and configure with actual item.
    id<HomeTableItemProtocol> item = [self.viewModel itemAtIndexPath:indexPath];
    if (item.isProcessed) {
        cell = [tableView dequeueReusableCellWithIdentifier:kImageCellID forIndexPath:indexPath];
        if (!cell) {
            cell = [[ImageTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kImageCellID];
        }
        [cell configureWithItem:item];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:kProgressCellID forIndexPath:indexPath];
        if (!cell) {
            cell = [[ProgressTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kProgressCellID];
        }
        [cell configureWithItem:item completion:^(BOOL success) {
            // After processing, replace ProcessTableCell with ImageTableCell.
            // indexPath can be changed when user remove some rows, so check it by item.
            NSIndexPath *actualIndexPath = [self.viewModel indexPathForItem:item];
            [tableView reloadRowsAtIndexPaths:@[actualIndexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![cell isKindOfClass:[ProgressTableCell class]]) {
        return;
    }
    
    id<HomeTableItemProtocol> item = [self.viewModel itemAtIndexPath:indexPath];
    ProgressTableCell *progressCell = (ProgressTableCell *)cell;
    progressCell.progressView.progress = [item currentProgress];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id<HomeTableItemProtocol> item = [self.viewModel itemAtIndexPath:indexPath];
    if (!item.isProcessed) {
        return;
    }
    
    NSString *saveTitle = @"Сохранить в галерею";
    UIAlertAction *save = [UIAlertAction actionWithTitle:saveTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            UIImageWriteToSavedPhotosAlbum(item.image, nil, nil, nil);
        });
    }];
    
    __weak typeof(self) wSelf = self;
    
    NSString *sourceTitle = @"Установить источником";
    UIAlertAction *source = [UIAlertAction actionWithTitle:sourceTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        __strong typeof(wSelf) sSelf = wSelf;
        if (!!sSelf) {
            sSelf.imageView.image = item.image;
        }
    }];
    
    NSString *removeTitle = @"Удалить изображение";
    UIAlertAction *remove = [UIAlertAction actionWithTitle:removeTitle style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        __strong typeof(wSelf) sSelf = wSelf;
        if (!!sSelf) {
            [sSelf.viewModel deleteItemAtIndexPath:indexPath];
            [sSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }];
    
    [self showObtainPhotoActionSheetWithActions:@[save, source, remove]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self.viewModel heightForRowAtIndexPath:indexPath];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (!!pickedImage) {
        self.pickerButton.hidden = YES;
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.image = pickedImage;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private Helpers
- (void)presentImagePicker:(UIImagePickerControllerSourceType)sourceType {
    self.imagePicker.sourceType = sourceType;
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

@end
