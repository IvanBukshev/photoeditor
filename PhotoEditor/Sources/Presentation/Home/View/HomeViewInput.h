//
//  HomeViewInput.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeViewInput <NSObject>

- (void)setupInitialState;
- (void)reloadTable;

@end
