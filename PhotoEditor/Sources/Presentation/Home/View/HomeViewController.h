//
//  HomeViewController.h
//  PhotoEditor
//
//  Created by Иван Букшев on 20/06/2017.
//  Copyright © 2017 Team Absurdum. All rights reserved.
//

#import "HomeViewInput.h"

@protocol HomeViewOutput;

@interface HomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, HomeViewInput>

@property (nonatomic, weak) IBOutlet UIButton *pickerButton;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) id<HomeViewOutput> viewModel;

@end
